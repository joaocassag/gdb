/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.gdb;

import com.afd.time.Config;
import java.util.Date;

import java.util.*;
import java.lang.*;
import org.hibernate.*;
//import gdb.utils.*;
import javax.persistence.CascadeType.*;
import com.afd.time.persistence.HibernateUtilSingleton;


/**
 *
 * @author cassamanojc
 */
public class gdb implements gdbI {
    
    Session session;
    String gdbuser;
    Date sqlmodifytime;
    String appname;
    SessionFactory sessionFactory;
    
    public gdb(Config cnf, String appname) throws Exception {
        this(cnf.getArg("db_sqldb_user"),appname,new Date());
        System.out.println("in sqldb; opening connection");
    }
    
    public gdb(String user, String appname, Date modifytime) throws Exception {
        System.out.println("start hibernate util; opening session");
        this.sessionFactory = HibernateUtilSingleton.getInstance().getSessionFactory();
        this.session = sessionFactory.openSession();
        System.out.println("start hibernate util; opened session");
        this.gdbuser = user;
        if (user == null) { this.gdbuser = HibernateUtilSingleton.getInstance().getUser();}
        this.sqlmodifytime = modifytime == null ? null : new Date(modifytime.getTime());
        this.appname = appname;
    }
    
    @Override
    public Session getSession() {
        if(session!=null && session.isOpen()) return session;
        
        if(sessionFactory ==null){
            this.sessionFactory = HibernateUtilSingleton.getInstance().getSessionFactory();
        }
        
        if(sessionFactory !=null && sessionFactory.isClosed()){
            this.sessionFactory = HibernateUtilSingleton.getInstance().getSessionFactory();
            
        }
        return sessionFactory.openSession();
        //return session==null?sessionFactory.openSession():session;
        //this.session = sessionFactory.openSession();
        //return session;
        
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getGdbuser() {
        return gdbuser;
    }

    public void setGdbuser(String gdbuser) {
        this.gdbuser = gdbuser;
    }

    public Date getSqlmodifytime() {
        return sqlmodifytime;
    }

    public void setSqlmodifytime(Date sqlmodifytime) {
        this.sqlmodifytime = sqlmodifytime;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }
    
    @Override
    public void close() {
        try {
            if (session != null) {session.close();}
            if(!HibernateUtilSingleton.getInstance().getSessionFactory().isClosed()) HibernateUtilSingleton.getInstance().shutdown();
        } catch (Exception ex) {/*we don't care*/}
    }
    
    @Override
    public void create(Session session, Object o ){
        Transaction tx = session.beginTransaction();
        session.save(o);
        tx.commit();
    }
    
    @Override
    public void delete(Session session, Object o ){
        Transaction tx = session.beginTransaction();
        session.delete(o);
        tx.commit();
    }
    
    @Override
    public void update(Session session, Object o ){
        Transaction tx = session.beginTransaction();
        session.update(o);
        tx.commit();
    }
    
    public List<Object> selectfromQuery(String hql, int n){
        Query query = getSession().createQuery(hql);
        return query.setMaxResults(n).list();
    }
    
    public List<Object> selectfromQuery(Query query, int n){
        //Query query = getSqlsession().createQuery(hql);
        return query.setMaxResults(n).list();
    }
    
}
