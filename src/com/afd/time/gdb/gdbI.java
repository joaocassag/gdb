/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.gdb;

import org.hibernate.Session;

/**
 *
 * @author cassamanojc
 */
public interface gdbI {
    
    public Session getSession();
    
    public void create(Session session, Object o);
    
    public void delete(Session session, Object o);
    
    public void update(Session session, Object o);
    
    public void close();
    
}
