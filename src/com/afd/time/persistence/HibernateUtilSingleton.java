package com.afd.time.persistence;

import com.afd.time.gdb.gdb;
import com.afd.time.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.sql.*;

import java.sql.SQLException;

public class HibernateUtilSingleton {

	protected final static Logger log = Logger.getLogger(HibernateUtilSingleton.class);
	private static Configuration conf;
	private static SessionFactory sessionFactory;
	private static String user;
        
        protected HibernateUtilSingleton(){}
        
        public static HibernateUtilSingleton getInstance(){
            if (instance == null) {
                synchronized(HibernateUtilSingleton.class) {
                    if (instance == null) {
                        instance = new HibernateUtilSingleton();
                    }
                }
            }
            return instance;
        }
        
        private static HibernateUtilSingleton instance = null;
        
	static {
		try {
			Config cnf = Config.getInstance();
			conf = new Configuration();

			//conf.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.EhCacheProvider");
			conf.setProperty("hibernate.connnection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
                        conf.setProperty("hibernate.connection.isolation", String.valueOf(Connection.TRANSACTION_SERIALIZABLE));
			conf.setProperty("hibernate.c3p0.min_size", "3");
			conf.setProperty("hibernate.c3p0.max_size", "250");
			conf.setProperty("hibernate.c3p0.max_statements", "50");
			// Test idle connections after 2 minutes.
			//conf.setProperty("hibernate.c3p0.idle_test_period", "120");
			// Kill idle connections after 5 minutes.
			//conf.setProperty("hibernate.c3p0.timeout", "300");
			
			conf.setProperty("hibernate.connection.driver_class", cnf.getArg("db_sqldb_driver"));
                        //conf.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
                        //conf.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver"); //good
			String url = cnf.getArg("db_sqldb_jdbcurl");
                        //String url = "jdbc:mysql://localhost:3306/regie";
                        //String url = "jdbc:mysql://localhost:3306/regie";//good
                        //String url = "jdbc:mysql://127.0.0.1:3306/regie?useSSL=false";
			//if (!url.contains(";SendStringParametersAsUnicode=false")) {
			//	url = url + ";SendStringParametersAsUnicode=false";
			//}
			conf.setProperty("hibernate.connection.url", url);
			conf.setProperty("hibernate.connection.username", cnf.getArg("db_sqldb_user"));
                        //conf.setProperty("hibernate.connection.username", "root");//good
			user = cnf.getArg("db_sqldb_user");
			//conf.setProperty("hibernate.connection.password", cnf.getArg("db_sqldb_password"));
                        conf.setProperty("hibernate.connection.password", "rootpass");
			conf.setProperty("hibernate.jdbc.batch_size", "30");
			conf.setProperty("hibernate.cache.use_second_level_cache", "false");
			//conf.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServer2008Dialect");
                        conf.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
                        

			conf.addAnnotatedClass(com.afd.time.model.Account.class);
                        conf.addAnnotatedClass(com.afd.time.model.Affiliation.class);
                        conf.addAnnotatedClass(com.afd.time.model.Course.class);
                        conf.addAnnotatedClass(com.afd.time.model.Enrollment.class);
                        conf.addAnnotatedClass(com.afd.time.model.Location.class);
                        conf.addAnnotatedClass(com.afd.time.model.Individual.class);
                        conf.addAnnotatedClass(com.afd.time.model.Term.class);
			
			// SessionFactory will be built, if necessary, in getSessionFactory()
		} catch (Throwable ex) {
			System.err.println(ex.getMessage());
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory(String appName) {
		if (null == sessionFactory) {

			// Set appName (if present)
			String key = "hibernate.connection.url";
			if (StringUtils.isNotBlank(appName)) {
				String value = ";appName=" + appName;
				String url = conf.getProperty(key);
				if (StringUtils.isNotBlank(url) && !url.contains(value)) {
					url += value;
				}
				conf.setProperty(key, url);
			}

			sessionFactory = conf.buildSessionFactory();
		}

		return sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
            sessionFactory = conf.buildSessionFactory();
            return sessionFactory;
	}

	public String getUser() {
		return user;
	}

	public void shutdown() throws SQLException {
		getSessionFactory().close();
	}

        /*
	public static void main(String[] args) throws Exception {
            System.out.println("Starting HibernateUtilSingleton!");
            getSessionFactory();
            System.out.println("Started HibernateUtilSingleton!");
            shutdown();

	}
        */
}
