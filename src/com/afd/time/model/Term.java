/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.model;

import javax.persistence.*;

/**
 *
 * @author cassamanojc
 */
@Entity

public class Term {
    
    @Id
    Long termid;
    String quarter;
    int quarterid;
    long year;
    String nextquarter;
    long nextyear;
    int nextquarterid;
    
    public int getQuarterid() {
        return quarterid;
    }

    public void setQuarterid(int quarterid) {
        this.quarterid = quarterid;
    }

    public int getNextquarterid() {
        return nextquarterid;
    }

    public void setNextquarterid(int nextquarterid) {
        this.nextquarterid = nextquarterid;
    }

    public Long getTermid() {
        return termid;
    }

    public void setTermid(Long termid) {
        this.termid = termid;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getNextquarter() {
        return nextquarter;
    }

    public void setNextquarter(String nextquarter) {
        this.nextquarter = nextquarter;
    }

    public long getNextyear() {
        return nextyear;
    }

    public void setNextyear(long nextyear) {
        this.nextyear = nextyear;
    }

   

    
}
