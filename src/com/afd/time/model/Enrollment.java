/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.model;

import javax.persistence.*;
/**
 *
 * @author cassamanojc_old
 */
@Entity
public class Enrollment {
    
    @Id
    long enrollmentid;
    int courseid;
    int personid;
    int quarterid;

    public int getQuarterid() {
        return quarterid;
    }

    public void setQuarterid(int quarterid) {
        this.quarterid = quarterid;
    }
    
    public long getEnrollmentid() {
        return enrollmentid;
    }

    public void setEnrollmentid(long enrollmentid) {
        this.enrollmentid = enrollmentid;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    
}
