
package com.afd.time.model;

import javax.persistence.*;

/**
 *
 * @author cassamanojc
 */
@Entity
public class Account {
    
    @Id
    String username;
    String password;
    boolean active;
    int accounttypeid;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", insertable = false, updatable = false)
    Individual person;
    int personid;

    public Individual getPerson() {
        return person;
    }

    public void setPerson(Individual person) {
        this.person = person;
    }

    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getAccounttypeid() {
        return accounttypeid;
    }

    public void setAccounttypeid(int accounttypeid) {
        this.accounttypeid = accounttypeid;
    }
    
    
}
