/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.model;
import javax.persistence.*;
/**
 *
 * @author cassamanojc
 */
@Entity
public class Location {
    
    @Id
    int locationid;
    String locationidDesc;
    String building;
    String room;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "locationid", insertable = false, updatable = false)
    Course course;
    
    @Transient
    Address address;
    
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    public int locationid() {
        return locationid;
    }

    public void setLocationid(int id) {
        this.locationid = id;
    }

    public String getLocationidDesc() {
        return locationidDesc;
    }

    public void setLocationidDesc(String locationid) {
        this.locationidDesc = locationid;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
