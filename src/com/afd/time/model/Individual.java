
package com.afd.time.model;
import java.util.*;
import javax.persistence.*;
/**
 *
 * @author cassamanojc
 */
@Entity
public class Individual {
    
    @Id
    int individualid;
    String firstname;
    String lastname;
    @Transient
    String middlename;
    Date birthdate;
    String uniqueid;
    Integer studentid;
    Integer employeeid;
    
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "individualid")
    List<Affiliation> affiliations;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "individualid")
    protected List<Account> accountlist;
    
    @Transient
    boolean student;
    @Transient
    boolean faculty;
    @Transient
    boolean instructor;
    @Transient
    boolean TA;

    public int getIndividualid() {
        return individualid;
    }

    public void setIndividualid(int individualid) {
        this.individualid = individualid;
    }
    
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }

    public Integer getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(Integer employeeid) {
        this.employeeid = employeeid;
    }

    public List<Affiliation> getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(List<Affiliation> affiliations) {
        this.affiliations = affiliations;
    }

    public boolean isStudent() {
        return student;
    }

    public void setStudent(boolean student) {
        this.student = student;
    }

    public boolean isFaculty() {
        return faculty;
    }

    public void setFaculty(boolean faculty) {
        this.faculty = faculty;
    }

    public boolean isInstructor() {
        return instructor;
    }

    public void setInstructor(boolean instructor) {
        this.instructor = instructor;
    }

    public boolean isTA() {
        return TA;
    }

    public void setTA(boolean TA) {
        this.TA = TA;
    }
    
    
}
