/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.model;
import java.util.*;
import javax.persistence.*;


/**
 *
 * @author cassamanojc
 */
@Entity
public class Affiliation {
    
    @Id
    long personid;
    String affiliation;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", insertable = false, updatable = false)
    Individual person;
    //Long personid;
    

    public long getPersonid() {
        return personid;
    }

    public void setPersonid(long personid) {
        this.personid = personid;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }
}
