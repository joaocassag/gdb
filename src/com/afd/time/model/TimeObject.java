/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.afd.time.model;

/**
 *
 * @author cassamanojc_old
 */
class TimeObject {
    
    long hour;
    long minutes;

    public TimeObject(String time){
        String[] hour_minutes = time.split(":");
        try{
            hour = Long.parseLong(hour_minutes[0]);
            minutes = Long.parseLong(hour_minutes[1]);
        }catch(NumberFormatException e){
            System.out.println("Warning: "+e.getMessage());
            hour=-1;
            minutes=-1;
        }
        
    }
    
    public long getHour() {
        return hour;
    }

    public void setHour(long hour) {
        this.hour = hour;
    }

    public long getMinutes() {
        return minutes;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }
}
