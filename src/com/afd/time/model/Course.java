
package com.afd.time.model;

import java.util.List;
import javax.persistence.*;
/**
 *
 * @author cassamanojc
 */
@Entity
public class Course {
    
    @Id
    int id;

    String coursename;
    String subject;
    String section;
    String day;
    long capacity;
    long enrolled;
    boolean registration; //ie registration season: open/closed
    //@OneToMany(mappedBy="college", fetch=FetchType.EAGER)
    //@ManyToOne
    //@JoinColumn(name="")
    //@OneToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "locationid")
    @Transient
    Location location;
    int locationid;
    
    //List<Location> location;
    String division; //ie PSD
    String department; //ie MPCS
    String quarter;
    long year;
    int quarterid;
    String startTime; // ie 17:30
    String endTime; // ie 08:30
    String instructor;
    boolean requiresLab;
    int minNumberOfLabs;
    //List<String> labs;
    boolean requiresInstructorConsent;
    
    public int getQuarterid() {
        return quarterid;
    }

    public void setQuarterid(int quarterid) {
        this.quarterid = quarterid;
    }
    
    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }
    
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public long getEnrolled() {
        return enrolled;
    }

    public void setEnrollred(long enrollred) {
        this.enrolled = enrollred;
    }

    public boolean isRegistration() {
        return registration;
    }

    public void setRegistration(boolean registration) {
        this.registration = registration;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    
    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }
    
    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStarttTime() {
        return startTime;
    }

    public void setStarttTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public boolean isRequiresLab() {
        return requiresLab;
    }

    public void setRequiresLab(boolean requiresLab) {
        this.requiresLab = requiresLab;
    }

    public int getMinNumberOfLabs() {
        return minNumberOfLabs;
    }

    public void setMinNumberOfLabs(int minNumberOfLabs) {
        this.minNumberOfLabs = minNumberOfLabs;
    }

    public boolean isRequiresInstructorConsent() {
        return requiresInstructorConsent;
    }

    public void setRequiresInstructorConsent(boolean requiresInstructorConsent) {
        this.requiresInstructorConsent = requiresInstructorConsent;
    }
    
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
